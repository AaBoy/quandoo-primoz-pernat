#Android Engineer Coding Challenge for Quandoo

In this coding challenge, I had to create an app that fetches data from an API and displays it in a grid of 3 columns wide. On item click, the details of the item opened in a new view. Usage of kotlin and MVVM were mandatory. 

In used the **MVVM** with repository to achieve separation of concern. For the API calls, I used retrofit and coroutines.  For DI I used Dagger 2.

Options were to add an endless scroll, landscape mode and offline data storage.  I used **Room** for data storage, **Paging Library** for paging and designed the layout so that all data is visible in landscape and portrait mode. 

I added tests to the project, I tested the paging part of the app.