package com.pp.quandoo.network

import androidx.lifecycle.LiveData
import com.pp.quandoo.model.Merchant
import com.pp.quandoo.model.MerchantResponse
import retrofit2.http.GET
import retrofit2.http.Query


interface QuandooApi {

    @GET("merchants")
    suspend fun getMerchantListPaging(
        @Query("offset") offset: Int,
        @Query("limit") limit: Int
    ): MerchantResponse

}