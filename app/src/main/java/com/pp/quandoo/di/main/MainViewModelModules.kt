package com.pp.quandoo.di.main

import androidx.lifecycle.ViewModel
import com.pp.quandoo.ui.list_merchants.MerchantListViewModel
import com.pp.quandoo.util.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface MainViewModelModules {

    @Binds
    @IntoMap
    @ViewModelKey(MerchantListViewModel::class)
    fun bindHomeViewModel(viewModel: MerchantListViewModel): ViewModel
}
