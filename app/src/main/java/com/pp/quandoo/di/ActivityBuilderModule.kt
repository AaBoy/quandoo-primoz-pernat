package com.pp.quandoo.di

import com.pp.quandoo.di.main.MainViewModelModules
import com.pp.quandoo.ui.list_merchants.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {

    @ContributesAndroidInjector(
        modules = [MainViewModelModules::class]
    )
    internal abstract fun contributeMainActivity(): MainActivity
}