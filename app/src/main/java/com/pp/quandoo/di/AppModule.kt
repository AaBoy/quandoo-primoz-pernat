package com.pp.quandoo.di

import android.app.Application
import androidx.room.Room
import com.pp.quandoo.db.MerchantDatabase
import com.pp.quandoo.model.MerchantJsonAdapter
import com.pp.quandoo.network.QuandooApi
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
object AppModule {

    @Singleton
    @JvmStatic
    @Provides
    fun provideRoomDatabase(application: Application): MerchantDatabase =
        Room.databaseBuilder(
            application.applicationContext,
            MerchantDatabase::class.java, MerchantDatabase.MERCHANT_DATABASE_NAME
        ).build()

    @Singleton
    @JvmStatic
    @Provides
    fun provideRetrofitInstance(moshi: Moshi): Retrofit = Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .baseUrl("https://api.quandoo.com/v1/")
        .build()

    @Provides
    @Singleton
    @JvmStatic
    fun provideQuandooApi(retrofit: Retrofit) = retrofit.create(QuandooApi::class.java)

    @Provides
    @Singleton
    @JvmStatic
    fun provideMoshiCutomAdatper() = Moshi.Builder().add(MerchantJsonAdapter()).build()

    @Provides
    @Singleton
    @JvmStatic
    fun provideBoundaryCoroutineScope() = CoroutineScope(Dispatchers.Main)
}