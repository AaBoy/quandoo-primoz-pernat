package com.pp.quandoo.util

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.pp.quandoo.R
import com.squareup.picasso.Picasso

@BindingAdapter("image")
fun loadImage(imageView: ImageView, url: String?) {
    Picasso.get().load(url).error(R.drawable.berlin).placeholder(R.drawable.gray_placeholder)
        .into(imageView)
}