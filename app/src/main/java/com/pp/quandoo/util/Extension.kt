package com.pp.quandoo.util

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.android.support.DaggerAppCompatActivity
import dagger.android.support.DaggerFragment

inline fun <reified T : ViewModel> DaggerFragment.getViewModel(
    factory: ViewModelProviderFactory
) = ViewModelProvider(this, factory)
    .get(T::class.java)

inline fun <reified T : ViewModel> DaggerAppCompatActivity.getViewModel(
    factory: ViewModelProviderFactory
) = ViewModelProvider(this, factory)
    .get(T::class.java)