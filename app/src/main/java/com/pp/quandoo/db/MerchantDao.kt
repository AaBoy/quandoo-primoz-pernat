package com.pp.quandoo.db

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.pp.quandoo.model.Merchant

@Dao
interface MerchantDao {

    @Query("SELECT * FROM merchant")
    fun getAllMerchant(): DataSource.Factory<Int, Merchant>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMerchant(merchant: List<Merchant>)

    @Query("SELECT COUNT(*) FROM merchant")
    suspend fun getCountInDB(): Int
}