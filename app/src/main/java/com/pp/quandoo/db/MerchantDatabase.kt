package com.pp.quandoo.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.pp.quandoo.model.*

@Database(
    entities = [Merchant::class, Location::class, Address::class, Coordinates::class],
    version = 1,
    exportSchema = false
)
abstract class MerchantDatabase : RoomDatabase() {
    companion object {
        final val MERCHANT_DATABASE_NAME = "merchant_database"
    }

    abstract fun merchantDao(): MerchantDao
}