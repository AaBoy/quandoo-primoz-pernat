package com.pp.quandoo.ui.list_merchants

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.paging.PagedList
import com.pp.quandoo.model.Merchant
import com.pp.quandoo.model.Result
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import javax.inject.Inject

class MerchantListViewModel @Inject constructor(
    private val repo: MerchantListRepository,
    private val coroutineScope: CoroutineScope
) : ViewModel() {

    private val testData: LiveData<Result<PagedList<Merchant>>> = liveData {
        emit(repo.getMerchants())
    }

    val data: LiveData<PagedList<Merchant>> = Transformations.switchMap(testData) {
        it.data
    }

    val error: LiveData<String> = Transformations.switchMap(testData) {
        it.networkErrors
    }

    override fun onCleared() {
        super.onCleared()
        coroutineScope.cancel()
    }
}