package com.pp.quandoo.ui.list_merchants

import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.pp.quandoo.db.MerchantDatabase
import com.pp.quandoo.model.Merchant
import com.pp.quandoo.model.Result
import javax.inject.Inject

class MerchantListRepository @Inject constructor(
    private val db: MerchantDatabase,
    private val boundaryCallback: MerchantBoundaryCallback
) {

    fun getMerchants(): Result<PagedList<Merchant>> {

        val dataSourceFactory = db.merchantDao().getAllMerchant()

        val data = LivePagedListBuilder(dataSourceFactory, PAGE_SIZE)
            .setBoundaryCallback(boundaryCallback)
            .build()

        return Result<PagedList<Merchant>>(data, boundaryCallback.errorMessage)
    }

    companion object {
        const val PAGE_SIZE = 30
    }
}