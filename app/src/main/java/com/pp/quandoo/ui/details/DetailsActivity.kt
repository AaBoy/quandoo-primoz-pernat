package com.pp.quandoo.ui.details

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.pp.quandoo.R
import com.pp.quandoo.databinding.ActivityDetailsBinding
import com.pp.quandoo.model.Merchant

class DetailsActivity : AppCompatActivity() {

    companion object {
        val MERCHANT_KEY = "merchant_key"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding =
            DataBindingUtil.setContentView<ActivityDetailsBinding>(this, R.layout.activity_details)

        val merchant = intent.extras?.getParcelable<Merchant>(MERCHANT_KEY)

        if (merchant != null) {
            binding.merchant = merchant
        }
    }

}
