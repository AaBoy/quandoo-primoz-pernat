package com.pp.quandoo.ui.list_merchants

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.pp.quandoo.R
import com.pp.quandoo.databinding.MerchantItemBinding
import com.pp.quandoo.model.Merchant

class MerchantAdapter(private val clickListener: MerchantClickListener) :
    PagedListAdapter<Merchant, MerchantViewHolder>(MERCHANT_COMPERATOR) {

    override fun onBindViewHolder(holder: MerchantViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bindData(it, clickListener)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MerchantViewHolder {
        val binding = DataBindingUtil.inflate<MerchantItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.merchant_item, parent, false
        )

        return MerchantViewHolder(binding)
    }

    companion object {
        private val MERCHANT_COMPERATOR = object : DiffUtil.ItemCallback<Merchant>() {
            override fun areItemsTheSame(oldItem: Merchant, newItem: Merchant): Boolean =
                oldItem.idTest == newItem.idTest

            override fun areContentsTheSame(oldItem: Merchant, newItem: Merchant): Boolean =
                oldItem == newItem

        }
    }
}

class MerchantViewHolder(private val binding: MerchantItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bindData(item: Merchant, clickListener: MerchantClickListener) {
        binding.merchant = item
        binding.clickListener = clickListener
        binding.notifyChange()
    }
}

class MerchantClickListener(val clickListener: (merchant: Merchant) -> Unit) {
    fun onClick(merchant: Merchant) = clickListener(merchant)
}