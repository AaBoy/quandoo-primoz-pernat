package com.pp.quandoo.ui.list_merchants

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import com.pp.quandoo.db.MerchantDatabase
import com.pp.quandoo.model.Merchant
import com.pp.quandoo.network.QuandooApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.lang.reflect.UndeclaredThrowableException
import javax.inject.Inject

class MerchantBoundaryCallback @Inject constructor(
    private val api: QuandooApi,
    private val db: MerchantDatabase,
    private val coroutineScope: CoroutineScope
) : PagedList.BoundaryCallback<Merchant>() {

    private val LIMIT_SECOND_FETCH: Int = 20
    private val OFFSET_SECOND_FETCH: Int = 100
    private val LIMIT_FIRST_FETCH: Int = 100
    private val OFFSET_FIRST_FETCH: Int = 0

    private var isRequestInProgress = false

    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String>
        get() = _errorMessage

    override fun onZeroItemsLoaded() {
        requestAndSave120Items()
    }

    override fun onItemAtEndLoaded(itemAtEnd: Merchant) {
        requestAndSaveData()
    }

    private fun requestAndSaveData() {
        if (isRequestInProgress) return

        isRequestInProgress = true

        CoroutineScope(coroutineScope.coroutineContext).launch {
            try {
                val data = api.getMerchantListPaging(db.merchantDao().getCountInDB(), 30)
                db.merchantDao().insertMerchant(data.merchants)
                isRequestInProgress = false
            } catch (e: UndeclaredThrowableException) {
                _errorMessage.postValue(e.undeclaredThrowable.message)
            } catch (e: Exception) {
                _errorMessage.postValue(e.message)
            } finally {
                isRequestInProgress = false
            }
        }
    }

    private fun requestAndSave120Items() {

        if (isRequestInProgress) return
        isRequestInProgress = true

        CoroutineScope(coroutineScope.coroutineContext).launch {
            try {
                //we have to do two fetches to be able to get 120 from the API
                val data100 =
                    async { api.getMerchantListPaging(OFFSET_FIRST_FETCH, LIMIT_FIRST_FETCH) }
                val data20 =
                    async { api.getMerchantListPaging(OFFSET_SECOND_FETCH, LIMIT_SECOND_FETCH) }

                val finalList = data100.await().merchants.union(data20.await().merchants)

                db.merchantDao().insertMerchant(finalList.toList())
            } catch (e: UndeclaredThrowableException) {
                _errorMessage.postValue(e.undeclaredThrowable.message)
            } catch (e: Exception) {
                _errorMessage.postValue(e.message)
            } finally {
                isRequestInProgress = false
            }
        }
    }
}