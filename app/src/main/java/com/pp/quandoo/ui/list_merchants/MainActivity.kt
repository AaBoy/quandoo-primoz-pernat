package com.pp.quandoo.ui.list_merchants

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import com.pp.quandoo.R
import com.pp.quandoo.databinding.ActivityMainBinding
import com.pp.quandoo.model.Merchant
import com.pp.quandoo.ui.details.DetailsActivity
import com.pp.quandoo.util.ViewModelProviderFactory
import com.pp.quandoo.util.getViewModel
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var providerFactory: ViewModelProviderFactory

    private val viewModel: MerchantListViewModel by lazy {
        getViewModel<MerchantListViewModel>(providerFactory)
    }

    private val binding: ActivityMainBinding by lazy {
        DataBindingUtil.setContentView<ActivityMainBinding>(
            this@MainActivity,
            R.layout.activity_main
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.lifecycleOwner = this
        registerObservers()
    }

    private fun registerObservers() {
        binding.recylverView.adapter = adapter
        viewModel.data.observe(this, Observer<PagedList<Merchant>> { list ->
                       if (list != null) {
                adapter.submitList(list)
            } else {
                Toast.makeText(this, "Empty List", Toast.LENGTH_SHORT).show()
            }

        })

        viewModel.error.observe(this, Observer<String> {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })
    }

    private val adapter = MerchantAdapter(MerchantClickListener {
        val bundle = Bundle()
        bundle.putParcelable(DetailsActivity.MERCHANT_KEY, it)

        val intent = Intent(this, DetailsActivity::class.java)
        intent.putExtra(DetailsActivity.MERCHANT_KEY, it)
        startActivity(intent)
    })
}
