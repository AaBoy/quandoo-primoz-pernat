package com.pp.quandoo.model

import android.os.Parcelable
import androidx.room.*
import kotlinx.android.parcel.Parcelize
import java.util.*
import java.util.Arrays.asList


@Parcelize
@Entity
data class Location(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "location_id")
    val id: Int,
    @Embedded
    val coordinates: Coordinates,
    @Embedded
    val address: Address
) : Parcelable


@kotlinx.android.parcel.Parcelize
@Entity
data class Coordinates(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "coordinates_id")
    val id: Int,
    val latitude: Double?,
    val longitude: Double?
) : Parcelable

@Parcelize
@Entity
data class Address(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "address_id")
    val id: Int,
    val street: String?,
    val number: String?,
    val zipcode: String?,
    val city: String?,
    val country: String?
) : Parcelable