package com.pp.quandoo.model

import androidx.lifecycle.LiveData

data class Result<T>(
    val data: LiveData<T>,
    val networkErrors: LiveData<String>
)