package com.pp.quandoo.model

import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.squareup.moshi.FromJson
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class Merchant(
    @PrimaryKey(autoGenerate = true)
    var idTest: Int?,
    val id: Int,
    val name: String,
    val images: String,
    val reviewScore: String,
    @Embedded
    val location: Location
) : Parcelable

data class MerchantResponse(val merchants: List<Merchant>)

// Created the JSON s we can get only the first image from the list and save to Room
data class MerchantJson(
    val id: Int,
    val name: String,
    val images: List<Images>,
    val reviewScore: String,
    val location: Location
)

data class Images(val url: String)

class MerchantJsonAdapter {
    @FromJson
    fun merchantFromJson(merchantJson: MerchantJson): Merchant {
        return Merchant(
            id = merchantJson.id,
            name = merchantJson.name,
            images = merchantJson.images[0].url,
            reviewScore = merchantJson.reviewScore,
            location = merchantJson.location,
            idTest = null
        );
    }
}