package com.pp.quandoo

import android.accounts.NetworkErrorException
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import com.pp.quandoo.db.MerchantDatabase
import com.pp.quandoo.model.MerchantResponse
import com.pp.quandoo.ui.list_merchants.MerchantBoundaryCallback
import com.pp.quandoo.network.QuandooApi
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.*
import org.junit.runner.RunWith
import java.lang.reflect.UndeclaredThrowableException

@RunWith(AndroidJUnit4::class)
class MerchantBoundaryCallbackInstrumentalTest {
    //constants
    val ITEMS_IN_EMPTY_DATABASE = 0
    val NORMAL_PAGE_INTERVAL = 30

    lateinit var db: MerchantDatabase
    val api = mockk<QuandooApi>()
    lateinit var merchantBoundaryCallback: MerchantBoundaryCallback

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDB() {
        db = Room.inMemoryDatabaseBuilder(
            InstrumentationRegistry.getInstrumentation().context, MerchantDatabase::class.java
        )
            .build()

        merchantBoundaryCallback =
            MerchantBoundaryCallback(api, db, CoroutineScope(Dispatchers.Unconfined))
    }

    @Test
    @Throws(Exception::class)
    fun normalInsertFlow() = runBlocking {
        //set the api return
        coEvery {
            api.getMerchantListPaging(
                ITEMS_IN_EMPTY_DATABASE,
                NORMAL_PAGE_INTERVAL
            )
        } returns MerchantResponse(SharedData.get30TestItems())

        merchantBoundaryCallback.onItemAtEndLoaded(SharedData.getTestMerchant())

        Assert.assertEquals(NORMAL_PAGE_INTERVAL, db.merchantDao().getCountInDB())
    }

    @Test
    fun api_returns_error_before_db_insert_error_intercepted_when_last_item_loaded() {
        val NETWORK_ERROR_MESSAGE = "Some network error"
        val exception = UndeclaredThrowableException(NetworkErrorException(NETWORK_ERROR_MESSAGE))

        //when api is called it returns an error
        coEvery {
            api.getMerchantListPaging(
                ITEMS_IN_EMPTY_DATABASE,
                NORMAL_PAGE_INTERVAL
            )
        } throws exception

        //Execute call
        merchantBoundaryCallback.onItemAtEndLoaded(SharedData.getTestMerchant())

        Assert.assertEquals(NETWORK_ERROR_MESSAGE, merchantBoundaryCallback.errorMessage.value)
    }

    @Test
    fun api_returns_error_before_db_insert_error_intercepted_when_first_item_fetched() {
        val NETWORK_ERROR_MESSAGE = "Some network error"
        val exception = UndeclaredThrowableException(NetworkErrorException(NETWORK_ERROR_MESSAGE))

        //when api is called it returns an error
        coEvery {
            api.getMerchantListPaging(
                ITEMS_IN_EMPTY_DATABASE,
                100
            )
        } throws exception

        //Execute call
        merchantBoundaryCallback.onZeroItemsLoaded()

        Assert.assertEquals(NETWORK_ERROR_MESSAGE, merchantBoundaryCallback.errorMessage.value)
    }

    @After
    fun closeDB() {
        db.close()
    }
}
