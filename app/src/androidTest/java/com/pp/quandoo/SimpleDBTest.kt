package com.pp.quandoo

import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import com.pp.quandoo.db.MerchantDao
import com.pp.quandoo.db.MerchantDatabase
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MerchantDBReadWriteTest {
    val NUMBER_OF_ITEMS_INSERTED = 30

    lateinit var db: MerchantDatabase
    lateinit var dao: MerchantDao

    @Before
    fun createDB() {

        db = Room.inMemoryDatabaseBuilder(
            InstrumentationRegistry.getInstrumentation().context, MerchantDatabase::class.java
        )
            .build()

        dao = db.merchantDao()
    }

    @Test
    fun insertToDB() = runBlocking<Unit> {
        //Perform insert into database
        dao.insertMerchant(SharedData.get30TestItems())

        //Check if the data is in the database
        Assert.assertEquals(NUMBER_OF_ITEMS_INSERTED, dao.getCountInDB())
    }

    @After
    fun closeDB() {
        db.close()
    }
}

