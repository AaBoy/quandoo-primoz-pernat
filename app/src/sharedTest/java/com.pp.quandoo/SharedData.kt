package com.pp.quandoo

import com.pp.quandoo.model.Address
import com.pp.quandoo.model.Coordinates
import com.pp.quandoo.model.Location
import com.pp.quandoo.model.Merchant

object SharedData {
    fun get100TestItems() = List<Merchant>(100) {

        val list = arrayListOf<Merchant>()

        for (x in 0 until 100) {
            list.add(getTestMerchant())
        }


        return list.toList()
    }

    fun get20TestItems() = List<Merchant>(20) {

        val list = arrayListOf<Merchant>()

        for (x in 0 until 20) {
            list.add(getTestMerchant())
        }


        return list.toList()
    }

    fun get30TestItems() = List<Merchant>(30) {

        val list = arrayListOf<Merchant>()

        for (x in 0 until 30) {
            list.add(getTestMerchant())
        }


        return list.toList()
    }


    fun getTestMerchant(): Merchant {
        return Merchant(
            null, 1, "Berlin Pizza", "https://image.com", "4.3",
            getTestLocation()
        )
    }

    fun getTestLocation(): Location {
        return Location(-1, getTestCoordinates(), getTestAddress())
    }

    fun getTestAddress(): Address {
        return Address(-1, "Berliner", "33", "1400", "Berlin", "DE")
    }

    fun getTestCoordinates(): Coordinates {
        return Coordinates(-1, 10.0, 10.0)
    }
}
