package com.pp.quandoo


import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.pp.quandoo.SharedData.get100TestItems
import com.pp.quandoo.SharedData.get20TestItems
import com.pp.quandoo.SharedData.get30TestItems
import com.pp.quandoo.db.MerchantDao
import com.pp.quandoo.db.MerchantDatabase
import com.pp.quandoo.model.MerchantResponse
import com.pp.quandoo.ui.list_merchants.MerchantBoundaryCallback
import com.pp.quandoo.network.QuandooApi
import io.mockk.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class MerchantBoundaryCallbackTest {

    lateinit var merchantBoundaryCallback: MerchantBoundaryCallback
    val api = mockk<QuandooApi>()
    val db = mockk<MerchantDatabase>()
    val dao = mockk<MerchantDao>()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun initData() {
        merchantBoundaryCallback =
            MerchantBoundaryCallback(api, db, CoroutineScope(Dispatchers.Unconfined))
    }

    @Test
    fun `insert 120 items for the first time`() = runBlocking {
        //set the data
        val responseOf100 = MerchantResponse(get100TestItems())
        val responseOf20 = MerchantResponse(get20TestItems())

        val finalList = responseOf100.merchants.union(responseOf20.merchants).toList()

        //set the api return
        coEvery { db.merchantDao() } returns dao
        coEvery { api.getMerchantListPaging(0, 100) } returns responseOf100
        coEvery { api.getMerchantListPaging(100, 20) } returns responseOf20
        coEvery { db.merchantDao().insertMerchant(finalList) }

        //Call function
        merchantBoundaryCallback.onZeroItemsLoaded()


        //verify dat db in being called
        coVerify { db.merchantDao().insertMerchant(finalList) }
    }

    @Test
    fun `fetch 30 items`() = runBlocking {
        //set up data
        val responseOf30 = MerchantResponse(get30TestItems())

        //set the api return and the db
        coEvery { db.merchantDao() } returns dao
        coEvery { db.merchantDao().getCountInDB() } returns 100

        coEvery {
            api.getMerchantListPaging(
                100,
                30
            )
        } returns responseOf30

        coEvery { db.merchantDao().insertMerchant(responseOf30.merchants) } returns Unit

        //start fetching and saving of the 30 items
        merchantBoundaryCallback.onItemAtEndLoaded(SharedData.getTestMerchant())

        //verify dat db in being called
        coVerify(exactly = 2) { db.merchantDao() }
        Assert.assertEquals(null, merchantBoundaryCallback.errorMessage.value)
    }
}